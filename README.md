# README #

### What is this repository for? ###

This repository contains the code for the example shown on the article "**Building Castles in the Sky.  Using JavaFX 3D to model historical treasures**" published in Oracle [Java Magazine](http://www.oraclejavamagazine-digital.com/javamagazine/current), issue from November/Dicember 2014.

### Acknowledgment ###

The basic 3D model for the Menéndez Pelayo Library, included in this repository has been gently donated by the Training and Employment Center (CEFEM) for the City Council of Santander in Cantabria, Spain.

### Requirements ###

* JDK 1.8. Get it from [here](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* No further dependencies are required

### Contributions ###

* Anyone is free to fork or clone this project
* Pull requests or issue notification will be welcome



José Pereda - [@JPeredaDnr](https://twitter.com/JPeredaDnr)
September 2014