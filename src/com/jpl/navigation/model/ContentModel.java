package com.jpl.navigation.model;

import com.javafx.experiments.jfx3dviewer.Xform;
import com.javafx.experiments.shape3d.PolygonMeshView;
import com.sun.javafx.geom.Vec3d;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Point3D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.image.Image;
import javafx.scene.input.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Affine;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

/**
 * 3D Content Model for Viewer App. 
 * Contains the 3D subscene and everything related to it: light, cameras, axis
 * Based on com.javafx.experiments.jfx3dviewer.ContentModel
 * 
 * @author Jose Pereda - @JPeredaDnr
 * Created on September - 2014
 */
public class ContentModel {
    
    private SubScene subScene;
    private ObservableList<Model3D> models;
    private final Group root3D = new Group();
    private final Group group3D = new Group();
    private final double paneW, paneH;
    private Point3D dimModel;
    
    private final PerspectiveCamera camera = new PerspectiveCamera(true);
    private final Rotate yUpRotate = new Rotate(180,0,0,0,Rotate.X_AXIS); // y Up
    private final Rotate cameraLookXRotate = new Rotate(0,0,0,0,Rotate.X_AXIS);
    private final Rotate cameraLookZRotate = new Rotate(0,0,0,0,Rotate.Z_AXIS);
    private final Translate cameraPosition = new Translate(0,0,0);
    private final Xform cameraXform = new Xform();
    private final Xform cameraXform2 = new Xform();
    private final PointLight light1 = new PointLight(Color.WHITE);
    private double mousePosX, mousePosY;
    private double mouseOldX, mouseOldY;
    private double mouseDeltaX, mouseDeltaY;
    private final double modifierFactor = 0.3;
         
    private final ObjectProperty<Node> content = new SimpleObjectProperty<>();
    private boolean removedTexture=false;
    private boolean keyHideTexture=false;
    private final ObjectProperty<Cursor> cursor = new SimpleObjectProperty<>(Cursor.DEFAULT);
    
    public ContentModel(ObservableList<Model3D> models, double paneW, double paneH) {
        this.models=models;
        this.paneW=paneW;
        this.paneH=paneH;
    
        Group root=new Group();
        models.stream().forEach(model->{
            model.wireframeProperty().addListener((ov,b,b1)->showWireframe(model,b1));
            
            Group g=new Group();
            g.setId(model.getId());
            g.getChildren().addAll(model.getMeshesSortedList(Comparator.comparing(Node::getId)));
            root.getChildren().add(g);
        });
        
        content.set(root);
        
        dimModel=new Point3D(root.getBoundsInParent().getWidth(),
                             root.getBoundsInParent().getHeight(), 
                             root.getBoundsInParent().getDepth());
        
        buildAxes();
        group3D.getChildren().add(root);
        
        buildCamera();
        addLights(); 
        
        buildSubScene();        
        
    }
    
    private void buildAxes() {
        double length = 2d*dimModel.magnitude();
        double width = dimModel.magnitude()/200d;
        double radius = 2d*dimModel.magnitude()/200d;
        final PhongMaterial redMaterial = new PhongMaterial();
        redMaterial.setDiffuseColor(Color.DARKRED);
        redMaterial.setSpecularColor(Color.RED);
        final PhongMaterial greenMaterial = new PhongMaterial();
        greenMaterial.setDiffuseColor(Color.DARKGREEN);
        greenMaterial.setSpecularColor(Color.GREEN);
        final PhongMaterial blueMaterial = new PhongMaterial();
        blueMaterial.setDiffuseColor(Color.DARKBLUE);
        blueMaterial.setSpecularColor(Color.BLUE);
        
        Sphere xSphere = new Sphere(radius);
        Sphere ySphere = new Sphere(radius);
        Sphere zSphere = new Sphere(radius);
        xSphere.setMaterial(redMaterial);
        ySphere.setMaterial(greenMaterial);
        zSphere.setMaterial(blueMaterial);
        
        xSphere.setTranslateX(dimModel.magnitude());
        ySphere.setTranslateY(dimModel.magnitude());
        zSphere.setTranslateZ(dimModel.magnitude());
        
        Box xAxis = new Box(length, width, width);
        Box yAxis = new Box(width, length, width);
        Box zAxis = new Box(width, width, length);
        xAxis.setMaterial(redMaterial);
        yAxis.setMaterial(greenMaterial);
        zAxis.setMaterial(blueMaterial);
        
        group3D.getChildren().addAll(xAxis, yAxis, zAxis);
        group3D.getChildren().addAll(xSphere, ySphere, zSphere);
    }
    
    private void buildCamera() {
        camera.setNearClip(1.0);
        camera.setFarClip(10000.0);
        camera.setFieldOfView(2d*dimModel.magnitude()/3d);
        camera.getTransforms().addAll(yUpRotate,cameraPosition,
                                      cameraLookXRotate,cameraLookZRotate);
        cameraXform.getChildren().add(cameraXform2);
        cameraXform2.getChildren().add(camera);
        cameraPosition.setZ(-2d*dimModel.magnitude());
        root3D.getChildren().add(cameraXform);
        
        cameraXform.setRx(-10.0);
        cameraXform.setRy(105.0);
        cameraXform.setTx(-5);
    }
    
    private void addLights(){
//        root3D.getChildren().add(ambientLight);
        root3D.getChildren().add(light1);
        light1.setTranslateX(0);
        light1.setTranslateY(1d*dimModel.getY()/3d);
        light1.setTranslateZ(-dimModel.getZ()/2);
    }

    private void buildSubScene() {
        root3D.getChildren().add(group3D);
        
        subScene = new SubScene(root3D,paneW,paneH,true,SceneAntialiasing.BALANCED);
        subScene.setCamera(camera);
        subScene.setFill(Color.CADETBLUE);
        setListeners(true);
    }
    
    private volatile boolean isPicking=false;
    private List<Node> modelMeshes;
    private Vec3d vecIni, vecPos;
    private double distance;
    
    private final EventHandler<MouseEvent> mouseEventHandler = event -> {
        double xFlip = -1.0, yFlip=1.0; // yUp
        if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
            mousePosX = event.getSceneX();
            mousePosY = event.getSceneY();
            mouseOldX = event.getSceneX();
            mouseOldY = event.getSceneY();
            // pick mesh
            PickResult pickResult = event.getPickResult();
            if(pickResult!=null){
                distance=pickResult.getIntersectedDistance();
            }
            String pickedModel=getModelId(event.getPickResult().getIntersectedNode());
            if(!pickedModel.isEmpty()){
                if(models.stream()
                      .filter(Model3D::isPickable)
                      .filter(m->m.getId().equals(pickedModel))
                      .anyMatch(m->{
                          modelMeshes=m.getMeshesList(); 
                          return true;
                      })){
                    isPicking=true;
                    cursor.set(Cursor.CLOSED_HAND);
                    vecIni = unProjectDirection(mousePosX, mousePosY, subScene.getWidth(),subScene.getHeight());
                }
            }
        } else if (event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
            mouseOldX = mousePosX;
            mouseOldY = mousePosY;
            mousePosX = event.getSceneX();
            mousePosY = event.getSceneY();
            mouseDeltaX = (mousePosX - mouseOldX);
            mouseDeltaY = (mousePosY - mouseOldY);
            
            if(isPicking){
                double modifier = event.isControlDown()?0.05:event.isAltDown()?1.0:0.2;
                vecPos = unProjectDirection(mousePosX, mousePosY, subScene.getWidth(),subScene.getHeight());
                if(modelMeshes!=null){
                    
                    /* 
                    EXPERIMENTAL! Work in progress to achieve movement of the selected model
                    Basic idea:
                     - Calculate the depth of the object: distance from the pick result
                     - Transform the mouse coordinates into projection space, with unProjectDirection
                     - Whenever the mouse moves transform it to projection space, compute the offset, 
                     - and transform to world coordinates to get the new object position
                    */
                    Point3D p=new Point3D(distance*(vecPos.x-vecIni.x),
                            distance*(vecPos.y-vecIni.y),distance*(vecPos.z-vecIni.z));
                    modelMeshes.forEach(n->{
                        // Restrict Y movement, so model keeps attached to ground!
                        n.getTransforms().add(new Translate(modifier*p.getX(),0,modifier*p.getZ()));
                    });
                    vecIni=vecPos;
                }
            } else {
                if(!keyHideTexture && !removedTexture){
                    showTextures(false);
                    removedTexture=true;
                }

                double modifier = event.isControlDown()?0.1:event.isShiftDown()?3.0:1.0;

                if(event.isMiddleButtonDown() || (event.isPrimaryButtonDown() && event.isSecondaryButtonDown())) {
                    cameraXform2.setTx(cameraXform2.t.getX() + xFlip*mouseDeltaX*modifierFactor*modifier*0.3); 
                    cameraXform2.setTy(cameraXform2.t.getY() + yFlip*mouseDeltaY*modifierFactor*modifier*0.3);
                }
                else if(event.isPrimaryButtonDown()) {
                    cameraXform.setRy(cameraXform.ry.getAngle() - yFlip*mouseDeltaX*modifierFactor*modifier*2.0);
                    cameraXform.setRx(cameraXform.rx.getAngle() + xFlip*mouseDeltaY*modifierFactor*modifier*2.0);
                }
                else if(event.isSecondaryButtonDown()) {
                    double z = cameraPosition.getZ();
                    double newZ = z - xFlip*(mouseDeltaX+mouseDeltaY)*modifierFactor*modifier;
                    cameraPosition.setZ(newZ);
                }
            }
        } else if (event.getEventType() == MouseEvent.MOUSE_RELEASED) {
            if(isPicking){
                cursor.set(Cursor.DEFAULT);
                // reset to avoid further transformations
                modelMeshes=null;
                isPicking=false;
            } else {
                if(!keyHideTexture){
                    // restore texture
                    showTextures(true);
                    removedTexture=false;
                }
            }
        }
    };
    
    private final EventHandler<ScrollEvent> scrollEventHandler = event -> {
        if (event.getTouchCount() > 0) { // touch pad scroll
            cameraXform2.setTx(cameraXform2.t.getX() - (0.01*event.getDeltaX()));  // -
            cameraXform2.setTy(cameraXform2.t.getY() + (0.01*event.getDeltaY()));  // -
        } else { // mouse wheel
            double z = cameraPosition.getZ()-(event.getDeltaY()*0.2);
            z = Math.max(z,-10d*dimModel.magnitude());
            z = Math.min(z,0);
            if(z!=cameraPosition.getZ()){
                if(!keyHideTexture && !removedTexture){
                    showTextures(false);
                    removedTexture=true;
                }
                cameraPosition.setZ(z);
                if(!keyHideTexture){
                    showTextures(true);
                    removedTexture=false;
                }
            }
        }
    };
    private final EventHandler<ZoomEvent> zoomEventHandler = event -> {
        if (!Double.isNaN(event.getZoomFactor()) && event.getZoomFactor() > 0.8 && event.getZoomFactor() < 1.2) {
            double z = cameraPosition.getZ()/event.getZoomFactor();
            z = Math.max(z,-10d*dimModel.magnitude());
            z = Math.min(z,0);
            cameraPosition.setZ(z);
        }
    };
    private final EventHandler<KeyEvent> keyEventHandler = (final KeyEvent event) -> {
        double CONTROL_MULTIPLIER = 0.1;
        double SHIFT_MULTIPLIER = 0.1;
        double ALT_MULTIPLIER = 0.5;
        switch (event.getCode()) {
            case R: /* Restore original position for all models */
                models.stream().forEach(m->{
                    Affine ori=m.getAffine();
                    m.getMeshesList().forEach(n->n.getTransforms().setAll(ori)); 
                });
                break;
            case H:  /* Hide all the textures */
                keyHideTexture=true;
                if(!removedTexture){
                    showTextures(false);
                    removedTexture=true;
                }
                break;
            case S: /* Show all the textures */
                keyHideTexture=false;
                if(removedTexture){
                    showTextures(true);
                    removedTexture=false;
                }
                break;
            case C: /* Reset cam */
                resetCam();
                break;
            case UP:
                if (event.isControlDown() && event.isShiftDown()) {
                    cameraXform2.t.setY(cameraXform2.t.getY() - 10.0*CONTROL_MULTIPLIER);  
                }  
                else if (event.isAltDown() && event.isShiftDown()) {
                    cameraXform.rx.setAngle(cameraXform.rx.getAngle() - 10.0*ALT_MULTIPLIER);  
                }
                else if (event.isControlDown()) {
                    cameraXform2.t.setY(cameraXform2.t.getY() - 1.0*CONTROL_MULTIPLIER);  
                }
                else if (event.isAltDown()) {
                    cameraXform.rx.setAngle(cameraXform.rx.getAngle() - 2.0*ALT_MULTIPLIER);  
                }
                else if (event.isShiftDown()) {
                    double z = camera.getTranslateZ();
                    double newZ = z + 5.0*SHIFT_MULTIPLIER;
                    camera.setTranslateZ(newZ);
                }
                break;
            case DOWN:
                if (event.isControlDown() && event.isShiftDown()) {
                    cameraXform2.t.setY(cameraXform2.t.getY() + 10.0*CONTROL_MULTIPLIER);  
                }  
                else if (event.isAltDown() && event.isShiftDown()) {
                    cameraXform.rx.setAngle(cameraXform.rx.getAngle() + 10.0*ALT_MULTIPLIER);  
                }
                else if (event.isControlDown()) {
                    cameraXform2.t.setY(cameraXform2.t.getY() + 1.0*CONTROL_MULTIPLIER);  
                }
                else if (event.isAltDown()) {
                    cameraXform.rx.setAngle(cameraXform.rx.getAngle() + 2.0*ALT_MULTIPLIER);  
                }
                else if (event.isShiftDown()) {
                    double z = camera.getTranslateZ();
                    double newZ = z - 5.0*SHIFT_MULTIPLIER;
                    camera.setTranslateZ(newZ);
                }
                break;
            case RIGHT:
                if (event.isControlDown() && event.isShiftDown()) {
                    cameraXform2.t.setX(cameraXform2.t.getX() + 10.0*CONTROL_MULTIPLIER);  
                } else if (event.isAltDown() && event.isShiftDown()) {
                    cameraXform.ry.setAngle(cameraXform.ry.getAngle() - 10.0*ALT_MULTIPLIER);  
                } else if (event.isControlDown()) {
                    cameraXform2.t.setX(cameraXform2.t.getX() + 1.0*CONTROL_MULTIPLIER);  
                } else if (event.isAltDown()) {
                    cameraXform.ry.setAngle(cameraXform.ry.getAngle() - 2.0*ALT_MULTIPLIER);  
                }
                break;
            case LEFT:
                if (event.isControlDown() && event.isShiftDown()) {
                    cameraXform2.t.setX(cameraXform2.t.getX() - 10.0*CONTROL_MULTIPLIER);  
                } else if (event.isAltDown() && event.isShiftDown()) {
                    cameraXform.ry.setAngle(cameraXform.ry.getAngle() + 10.0*ALT_MULTIPLIER);  // -
                } else if (event.isControlDown()) {
                    cameraXform2.t.setX(cameraXform2.t.getX() - 1.0*CONTROL_MULTIPLIER);  
                } else if (event.isAltDown()) {
                    cameraXform.ry.setAngle(cameraXform.ry.getAngle() + 2.0*ALT_MULTIPLIER);  // -
                }
                break;
        }
        event.consume();
    };
            
    private void setListeners(boolean addListeners){
        if(addListeners){
            subScene.addEventHandler(MouseEvent.ANY, mouseEventHandler);
            subScene.addEventHandler(KeyEvent.KEY_PRESSED, keyEventHandler);
            subScene.addEventHandler(ZoomEvent.ANY, zoomEventHandler);
            subScene.addEventHandler(ScrollEvent.ANY, scrollEventHandler);
        } else {
            subScene.removeEventHandler(MouseEvent.ANY, mouseEventHandler);
            subScene.removeEventHandler(KeyEvent.KEY_PRESSED, keyEventHandler);
            subScene.removeEventHandler(ZoomEvent.ANY, zoomEventHandler);
            subScene.removeEventHandler(ScrollEvent.ANY, scrollEventHandler);
        }
    }
    
    /*
    Public methods
    */
    public ObjectProperty<Node> contentProperty() { return content; }
    public Node getContent() { return content.get(); }
    
    public ObjectProperty<Cursor> getCursor() { return cursor; }
    public Group getGroup3D() { return group3D; }
    public SubScene getSubScene() { return subScene; }    
    public void resetCam(){
        cameraXform.ry.setAngle(0.0);
        cameraXform.rx.setAngle(0.0);
        cameraPosition.setZ(-2d*dimModel.magnitude());
        cameraXform2.t.setX(0.0);
        cameraXform2.t.setY(0.0);
        cameraXform.setRx(-10.0);
        cameraXform.setRy(105.0);
        cameraXform.setTx(-5);
    }

    private void showWireframe(Model3D model, boolean wireframe) {
        Platform.runLater(()->{
            model.getMeshesList().forEach(m->{
                if (m instanceof PolygonMeshView) {
                    ((PolygonMeshView)m).setDrawMode(wireframe ? DrawMode.LINE: DrawMode.FILL);
                } else if (m instanceof MeshView) {
                    ((MeshView)m).setDrawMode(wireframe ? DrawMode.LINE: DrawMode.FILL);
                } 
            });
        });
    }
    
    private void showTextures(boolean show){
        Platform.runLater(()->{
            models.forEach(model->{
                model.getMapMeshesModel().forEach((k,v)->{
                    PhongMaterial material=null;
                    if (v instanceof PolygonMeshView) {
                        material = (PhongMaterial)((PolygonMeshView)v).getMaterial();
                    } else if (v instanceof MeshView) {
                        material = (PhongMaterial)((MeshView)v).getMaterial();
                    }
                    if(material!=null){
                        if(!show){
                            material.setDiffuseMap(null);
                        } else {
                            Image imgOriginal = model.getImagesModel().get(k);
                            if(imgOriginal!=null){
                                material.setDiffuseMap(imgOriginal);
                            }
                        }
                    }
                });
            });
        });
    }
    
    private String getModelId(Node picked){
        if(picked==null || picked.getParent()==null || 
           !(picked instanceof MeshView || picked instanceof PolygonMeshView)){
            return "";
        }
        // picked mesh's parent is the model
        final Optional<String> opPicked1=Optional.ofNullable(picked.getParent().getId());
        if(opPicked1.isPresent() && 
           models.stream().filter(m->m.getId().equals(opPicked1.get())).findAny().isPresent()){
            return opPicked1.get();
        } 
        // picked mesh's parent's parent is the model
        final Optional<String> opPicked2=Optional.ofNullable(picked.getParent().getParent().getId());
        if(opPicked2.isPresent() && 
           models.stream().filter(m->m.getId().equals(opPicked2.get())).findAny().isPresent()){
            return opPicked2.get();
        }
        return "";
    }
    
    /*
     From fx83dfeatures.Camera3D
     http://hg.openjdk.java.net/openjfx/8u-dev/rt/file/f4e58490d406/apps/toys/FX8-3DFeatures/src/fx83dfeatures/Camera3D.java
    */
    /*
     * returns 3D direction from the Camera position to the mouse
     * in the Scene space 
     */
    
    public Vec3d unProjectDirection(double sceneX, double sceneY, double sWidth, double sHeight) {
        double tanHFov = Math.tan(Math.toRadians(camera.getFieldOfView()) * 0.5f);
        Vec3d vMouse = new Vec3d(2*sceneX/sWidth-1, 2*sceneY/sWidth-sHeight/sWidth, 1);
        vMouse.x *= tanHFov;
        vMouse.y *= tanHFov;

        Vec3d result = localToSceneDirection(vMouse, new Vec3d());
        result.normalize();
        return result;
    }
    
    public Vec3d localToScene(Vec3d pt, Vec3d result) {
        Point3D res = camera.localToParentTransformProperty().get().transform(pt.x, pt.y, pt.z);
        if (camera.getParent() != null) {
            res = camera.getParent().localToSceneTransformProperty().get().transform(res);
        }
        result.set(res.getX(), res.getY(), res.getZ());
        return result;
    }
    
    public Vec3d localToSceneDirection(Vec3d dir, Vec3d result) {
        localToScene(dir, result);
        result.sub(localToScene(new Vec3d(0, 0, 0), new Vec3d()));
        return result;
    }
}
