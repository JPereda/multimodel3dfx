package com.jpl.navigation.model;

import com.jpl.navigation.control.Settings;
import com.jpl.navigation.control.HierarchyModel;
import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.SubScene;
import javafx.scene.control.TreeTableView;
import javafx.scene.transform.Affine;
import javafx.scene.transform.Rotate;

/**
 *
 * @author Jose Pereda - @JPeredaDnr
 * Created on September - 2014
 */
public class Library {

    private final ContentModel content; 
    private final HierarchyModel hierarchy;
    private final Settings settings;
    
    private final ObservableList<Model3D> models=FXCollections.observableArrayList();
    
    public Library(){
        /*
        Model 1. Building - Outside
        */
        Model3D modelBuildingOut =new Model3D("lib_Walls.obj","Library Walls");
        modelBuildingOut.importObj();
        models.add(modelBuildingOut);
        
        /*
        Model 2. Building - Inside
        */
        Model3D modelBuildingIn =new Model3D("lib_Columns.obj","Library Columns");
        modelBuildingIn.setLoadAsPolygons(true);
        modelBuildingIn.importObj();
        models.add(modelBuildingIn);
        
        /*
        Model 3. Table
        */
        Model3D modelTable=new Model3D("table.obj","Table");
        Affine affineTable=new Affine();    
        affineTable.prependTranslation(0,0.63,-4);
        modelTable.setAffine(affineTable);
        modelTable.setPickable(false);
        modelTable.importObj();
        models.add(modelTable);
        
        /*
        Model 4. Chair - 1
        */
        final double scaleChair=0.138;
        
        Model3D modelChair1=new Model3D("chair.obj","Chair - 1");
        Affine affineChair=new Affine();    
        affineChair.appendScale(scaleChair,scaleChair,scaleChair);
        affineChair.appendTranslation(2,4.13,-12);
        modelChair1.setAffine(affineChair);
        modelChair1.importObj();
        modelChair1.setPickable(true);
        models.add(modelChair1);
        
        /*
        Model 5. Chair - 2
        */
        Model3D modelChair2=new Model3D("chair.obj","Chair - 2");
        Affine affineChair2=new Affine();    
        affineChair2.prependTranslation(2.3,4.13,-22);
        affineChair2.prependScale(scaleChair,scaleChair,scaleChair);
        modelChair2.setAffine(affineChair2);
        modelChair2.importObj();
        modelChair2.setPickable(true);
        models.add(modelChair2);
              
        /*
        Create content subscene, add library, set camera and lights
        */
        content = new ContentModel(models,1024,800); 
        
        /*
        Controls: Settings & Tree
        */
        settings=new Settings(models);
        hierarchy=new HierarchyModel(content.contentProperty());
    }
       
    public SubScene getSubScene(){ return content.getSubScene(); }
    public ObjectProperty<Cursor> getCursor() { return content.getCursor(); }
    
    public void clearSelection() { hierarchy.clearSelection(); }
    public TreeTableView<Node> getHierarchyTable() { return hierarchy.getHierarchyTable(); }
    public TreeTableView<Model3D> getSettingsTable() { return settings.getSettingsTable(); }
}
