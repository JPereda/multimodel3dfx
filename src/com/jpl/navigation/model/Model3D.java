package com.jpl.navigation.model;

import com.javafx.experiments.importers.obj.ObjImporter;
import com.javafx.experiments.importers.obj.PolyObjImporter;
import com.javafx.experiments.shape3d.PolygonMeshView;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.MeshView;
import javafx.scene.transform.Affine;

/**
 *
 * @author Jose Pereda - @JPeredaDnr
 * Created on September - 2014
 */
public class Model3D {
    
    /*
    HashMap to store a MeshView of each mesh with its key.
    */
    private final Map<String,Node> mapMeshesModel=new HashMap<>();
    private final Map<String,Image> mapImagesModel=new HashMap<>();
    
    private final String fileName;
    private final StringProperty id=new SimpleStringProperty();
    private boolean loadAsPolygons;
    private Affine affine;
    private final BooleanProperty pickable=new SimpleBooleanProperty(false);
    private final BooleanProperty wireframe=new SimpleBooleanProperty(false);
    
    public Model3D(String fileName, String id){
        this.fileName=fileName;
        this.id.set(id);
        this.loadAsPolygons=false;
        this.affine=new Affine(); 
    }
    
    public void setLoadAsPolygons(boolean loadAsPolygons){ this.loadAsPolygons=loadAsPolygons; }
    public boolean isLoadAsPolygons() { return this.loadAsPolygons; }
    
    public void setAffine(Affine affine){ this.affine=affine; }
    public Affine getAffine() { return this.affine; }
    
    public StringProperty idProperty() { return id; }
    public String getId() { return id.get(); }
    public void setId(String id) { this.id.set(id); } 
    
    public BooleanProperty pickableProperty() { return pickable; }
    public void setPickable(boolean pickable) { this.pickable.set(pickable); }
    public boolean isPickable() { return this.pickable.get(); }
    
    public BooleanProperty wireframeProperty() { return wireframe; }
    public void setWireframe(boolean wireframe) { this.wireframe.set(wireframe); }
    public boolean isWireframe() { return this.wireframe.get(); }
    
    public void importObj(){
        if(loadAsPolygons){
            try {
                PolyObjImporter reader = new PolyObjImporter(getClass().getResource("/resources/"+fileName).toExternalForm());
                Set<String> meshesModel=reader.getMeshes(); 
                meshesModel.forEach(s-> { 
                    PolygonMeshView meshPart = reader.buildPolygonMeshView(s);
                    meshPart.getTransforms().setAll(affine); 
                    PhongMaterial material = (PhongMaterial) meshPart.getMaterial();
                    material.setSpecularPower(10);
                    material.setSpecularColor(Color.WHITE);
                    meshPart.setMaterial(material);
                    mapImagesModel.put(s,material.getDiffuseMap()); 
                    mapMeshesModel.put(s,meshPart); 
                });
            } catch (IOException e) {
                System.out.println("Error loading model "+e.toString());
            }
        } else {
            try {
                ObjImporter reader = new ObjImporter(getClass().getResource("/resources/"+fileName).toExternalForm());
                Set<String> meshesModel=reader.getMeshes();
                meshesModel.forEach(s-> { 
                    MeshView meshPart = reader.buildMeshView(s);
                    meshPart.getTransforms().setAll(affine); 
                    PhongMaterial material = (PhongMaterial) meshPart.getMaterial();
                    material.setSpecularPower(10);
                    meshPart.setMaterial(material);
                    mapImagesModel.put(s,material.getDiffuseMap()); 
                    mapMeshesModel.put(s,meshPart); 
                });
            } catch (IOException e) {
                System.out.println("Error loading model "+e.toString());
            }
        }
    }

    public Map<String, Node> getMapMeshesModel() { return mapMeshesModel; }
    
    public List<Node> getMeshesList() {
        return mapMeshesModel.values().stream()
                .collect(Collectors.toList());
    }
    
    public List<Node> getMeshesSortedList(Comparator<Node> comparator) {
        return mapMeshesModel.values().stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }
    
    
    public Map<String, Image> getImagesModel() { return mapImagesModel; }

}
