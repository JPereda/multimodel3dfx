package com.jpl.navigation.control;

import com.jpl.navigation.model.Model3D;
import javafx.beans.binding.ObjectBinding;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.CheckBoxTreeTableCell;

/**
 *
 * @author Jose Pereda - @JPeredaDnr
 * Created on September - 2014
 */
public class Settings {

    private final TreeTableView<Model3D> settingsTreeTable;
    private final TreeTableColumn<Model3D, String> nodeColumn;
    private final TreeTableColumn<Model3D, Boolean> pickableColumn;
    private final TreeTableColumn<Model3D, Boolean> wireframeColumn;
            
    public Settings(ObservableList<Model3D> models){
        settingsTreeTable=new TreeTableView<>();
        settingsTreeTable.setEditable(true);
        nodeColumn=new TreeTableColumn<>("Node");
        nodeColumn.setEditable(false);
        pickableColumn=new TreeTableColumn<>("Pickable");
        pickableColumn.setEditable(true);
        wireframeColumn=new TreeTableColumn<>("Wireframe");
        wireframeColumn.setEditable(true);
        
        nodeColumn.setCellValueFactory(p -> p.getValue().getValue().idProperty());
        pickableColumn.setCellValueFactory(p -> {
            return p.getValue().getValue().pickableProperty();
        });
        pickableColumn.setCellFactory(CheckBoxTreeTableCell.forTreeTableColumn(pickableColumn));
        wireframeColumn.setCellValueFactory(p -> {
            return p.getValue().getValue().wireframeProperty();
        });
        wireframeColumn.setCellFactory(CheckBoxTreeTableCell.forTreeTableColumn(wireframeColumn));
        
        settingsTreeTable.getColumns().addAll(nodeColumn,pickableColumn,wireframeColumn);
        settingsTreeTable.showRootProperty().set(false);
        settingsTreeTable.rootProperty().bind(new ObjectBinding<TreeItem<Model3D>>() {

            {
                bind(models);
            }

            @Override
            protected TreeItem<Model3D> computeValue() {
                if (models != null) {
                    TreeItem<Model3D> root=new TreeItem<>();
                    models.forEach(s->{
                        root.getChildren().add(new TreeItem<>(s));
                    });
                    return root;
                }
                return null;
            }
        });
        
    }
    
    public TreeTableView<Model3D> getSettingsTable() { return settingsTreeTable; }
    
}
