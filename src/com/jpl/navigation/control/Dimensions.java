package com.jpl.navigation.control;

import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.TreeTableColumn;

/**
 *
 * @author Jose Pereda - @JPeredaDnr
 * Created on September - 2014
 */
public class Dimensions extends ObjectBinding<Double> {

    public enum DIR{ X,Y,Z }
    
    private final TreeTableColumn.CellDataFeatures<Node, Double> p;
    private final DIR dir;
    
    public Dimensions(TreeTableColumn.CellDataFeatures<Node, Double> p, DIR dir){
        this.p=p;
        this.dir=dir;
        bind(p.getValue().getValue().boundsInLocalProperty());
    }
    
    @Override
    protected Double computeValue() {
        DoubleProperty w0=new SimpleDoubleProperty(Double.MAX_VALUE);
        DoubleProperty w1=new SimpleDoubleProperty(-Double.MAX_VALUE);
        if(p.getValue().getValue() instanceof Group){
            p.getValue().getChildren().forEach(m->{
                if(m.getValue() instanceof Group){
                    m.getChildren().stream()
                        .forEach(m2->{
                            Bounds bounds = m2.getValue().getBoundsInLocal();
                            switch(dir){
                                case X: 
                                    w0.set(Math.min(w0.get(),bounds.getMinX()));
                                    w1.set(Math.max(w1.get(),bounds.getMaxX()));
                                    break;
                                case Y: 
                                    w0.set(Math.min(w0.get(),bounds.getMinY()));
                                    w1.set(Math.max(w1.get(),bounds.getMaxY()));
                                    break;
                                case Z: 
                                    w0.set(Math.min(w0.get(),bounds.getMinZ()));
                                    w1.set(Math.max(w1.get(),bounds.getMaxZ()));
                                    break;
                            }
                        });
                } else {
                    Bounds bounds = m.getValue().getBoundsInLocal();
                    switch(dir){
                            case X: 
                                w0.set(Math.min(w0.get(),bounds.getMinX()));
                                w1.set(Math.max(w1.get(),bounds.getMaxX()));
                                break;
                            case Y: 
                                w0.set(Math.min(w0.get(),bounds.getMinY()));
                                w1.set(Math.max(w1.get(),bounds.getMaxY()));
                                break;
                            case Z: 
                                w0.set(Math.min(w0.get(),bounds.getMinZ()));
                                w1.set(Math.max(w1.get(),bounds.getMaxZ()));
                                break;
                    }
                }
            });
            return w1.subtract(w0).getValue();
        }
        switch(dir){
            case X: 
                return p.getValue().getValue().getBoundsInLocal().getWidth();
            case Y: 
                return p.getValue().getValue().getBoundsInLocal().getHeight();
            case Z: 
                return p.getValue().getValue().getBoundsInLocal().getDepth();
        }
        return 0d;
    }

}
