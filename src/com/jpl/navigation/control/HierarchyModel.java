package com.jpl.navigation.control;

import com.javafx.experiments.shape3d.PolygonMeshView;
import com.jpl.navigation.control.Dimensions.DIR;
import java.util.List;
import java.util.stream.Collectors;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.CheckBoxTreeTableCell;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.MeshView;
import javafx.util.StringConverter;

/**
 *
 * @author Jose Pereda - @JPeredaDnr
 * Created on September - 2014
 */
public class HierarchyModel {

    private final TreeTableView<Node> hierarachyTreeTable;
    private final TreeTableColumn<Node, String> nodeColumn;
    private final TreeTableColumn<Node, String> idColumn;
    private final TreeTableColumn<Node, Boolean> visibilityColumn;
    private final TreeTableColumn<Node, Double> widthColumn;
    private final TreeTableColumn<Node, Double> heightColumn;
    private final TreeTableColumn<Node, Double> depthColumn;
    
    private final ChangeListener<TreeItem<Node>> treeListener;
    
    private Node nodeSelected;
    
    public HierarchyModel(ObjectProperty<Node> content){
                
        hierarachyTreeTable=new TreeTableView<>();
        hierarachyTreeTable.setEditable(true);
        nodeColumn=new TreeTableColumn<>("Node");
        nodeColumn.setEditable(false);
        idColumn=new TreeTableColumn<>("Id");
        idColumn.setEditable(false);
        visibilityColumn=new TreeTableColumn<>("Visibility");
        visibilityColumn.setEditable(true);
        widthColumn=new TreeTableColumn<>("W");
        widthColumn.setEditable(false);
        heightColumn=new TreeTableColumn<>("H");
        heightColumn.setEditable(false);
        depthColumn=new TreeTableColumn<>("D");
        depthColumn.setEditable(false);
        
        nodeColumn.setCellValueFactory(p -> p.getValue().valueProperty().asString());
        idColumn.setCellValueFactory(p -> p.getValue().getValue().idProperty());        
        visibilityColumn.setCellValueFactory(p -> {
            if(p.getValue().getValue() instanceof Group){
                p.getValue().getChildren().forEach(m->{
                    m.getValue().setVisible(p.getValue().getValue().visibleProperty().get());
                    if(m.getValue() instanceof Group){
                        m.getChildren().forEach(m2->{
                            m2.getValue().setVisible(p.getValue().getValue().visibleProperty().get());
                        });
                    }
                });
            }
            return p.getValue().getValue().visibleProperty();
        });
        visibilityColumn.setCellFactory(CheckBoxTreeTableCell.forTreeTableColumn(visibilityColumn));
        widthColumn.setCellValueFactory(p -> new Dimensions(p,DIR.X));
        heightColumn.setCellValueFactory(p -> new Dimensions(p,DIR.Y));
        depthColumn.setCellValueFactory(p -> new Dimensions(p,DIR.Z));
        
        StringConverter<Double> niceDoubleStringConverter = new StringConverter<Double>() {
            @Override
            public String toString(Double t) {
                return String.format("%.2f", t);
            }

            @Override
            public Double fromString(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //Not needed so far
            }
        };
        widthColumn.setCellFactory(TextFieldTreeTableCell.<Node, Double>forTreeTableColumn(niceDoubleStringConverter));
        heightColumn.setCellFactory(TextFieldTreeTableCell.<Node, Double>forTreeTableColumn(niceDoubleStringConverter));
        depthColumn.setCellFactory(TextFieldTreeTableCell.<Node, Double>forTreeTableColumn(niceDoubleStringConverter));
        
        hierarachyTreeTable.getColumns().addAll(nodeColumn,idColumn,visibilityColumn,widthColumn,heightColumn,depthColumn);
        hierarachyTreeTable.showRootProperty().set(false);
        hierarachyTreeTable.rootProperty().bind(new ObjectBinding<TreeItem<Node>>() {

            {
                bind(content);
            }

            @Override
            protected TreeItem<Node> computeValue() {
                Group models3D =(Group) content.get();
                if (models3D != null) {
                    TreeItem<Node> root=new TreeItem<>(models3D);
                    models3D.getChildren().stream()
                            .map(n->(Group)n)
                            .forEach(g->{
                                MeshesTree tree = new MeshesTree(g,g.getId());
                                root.getChildren().add(tree.getTreeIndex());
                            });
                    return root;
                }
                return null;
            }
        });
        
        // treeitem selected-> the meshview/polygonmeshview is highlighted
        treeListener=(ov,t,t1)->{
            clearSelection();
            if(t1!=null && t1.getValue()!=null){
                Node n= t1.getValue();
                System.out.println("n: "+n.getId()+" "+t1.getParent().toString());
                if (n instanceof MeshView) {
                    PhongMaterial material = (PhongMaterial)((MeshView)n).getMaterial();
                    PhongMaterial mat=new PhongMaterial();
                    mat.setSpecularPower(material.getSpecularPower());
                    mat.setSpecularColor(material.getSpecularColor());
                    mat.setDiffuseMap(material.getDiffuseMap());
                    mat.setSelfIlluminationMap(new Image(getClass().getResourceAsStream("/resources/red.png")));
                    ((MeshView)n).setMaterial(mat);
                    nodeSelected=n;
                } else if (n instanceof PolygonMeshView) {
                    PhongMaterial material = (PhongMaterial)((PolygonMeshView)n).getMaterial();
                    PhongMaterial mat=new PhongMaterial();
                    mat.setSpecularPower(material.getSpecularPower());
                    mat.setSpecularColor(material.getSpecularColor());
                    mat.setDiffuseMap(material.getDiffuseMap());
                    mat.setSelfIlluminationMap(new Image(getClass().getResourceAsStream("/resources/red.png")));
                    ((PolygonMeshView)n).setMaterial(mat);
                    nodeSelected=n;
                } 
            }
        };
        
        hierarachyTreeTable.getSelectionModel().selectedItemProperty().addListener(treeListener);
        
        // listener to space key pressed, set visible/not visible selected item node and children
        hierarachyTreeTable.setOnKeyPressed(t -> {
            if (t.getCode() == KeyCode.SPACE) {
                TreeItem<Node> selectedItem = hierarachyTreeTable.getSelectionModel().getSelectedItem();
                if (selectedItem != null) {
                    Node node = selectedItem.getValue();
                    node.setVisible(!node.isVisible());
                    if(node instanceof Group){
                        selectedItem.getChildren().forEach(m->{
                            m.getValue().setVisible(node.isVisible());
                            if(m.getValue() instanceof Group){
                                m.getChildren().forEach(m2->{
                                    m2.getValue().setVisible(node.isVisible());
                                });
                            }
                        });
                    }
                }
                t.consume();
            }
        });
    }

    public final void clearSelection() {
        if(nodeSelected!=null){
            if (nodeSelected instanceof MeshView) {
                PhongMaterial material = (PhongMaterial)((MeshView)nodeSelected).getMaterial();
                material.setSelfIlluminationMap(null);
            } else if (nodeSelected instanceof PolygonMeshView) {
                PhongMaterial material = (PhongMaterial)((PolygonMeshView)nodeSelected).getMaterial();
                material.setSelfIlluminationMap(null);
            }
            nodeSelected=null;
        }
    }
    
    private class MeshesTree {
    
        private final TreeItem<Node> treeIndex;
    
        public MeshesTree(Group node, String name){
            
            // first level: groups
            List<TreeItem<Node>> treeTitle = node.getChildren().stream()
                .map(m->getFirstName(m.getId()))
                .distinct()
                .map(s->{
                    Node m=new Group();
                    m.setId(s);
                    return new TreeItem<>(m);
                }).collect(Collectors.toList());

            // second level: meshview/polygonmeshview
            treeTitle.stream().forEach(t->{
                node.getChildren().stream()
                    .filter(m->getFirstName(m.getId()).equals(t.getValue().getId()))
                    .forEach(m->{
                        TreeItem<Node> item = new TreeItem<>(m);
                        // mouse click listener for every meshview: when a mesh is picked, 
                        // its treeitem is selected, and the treeview is scrolled to that item
                        m.setOnMouseClicked(e -> {
                            TreeItem<Node> p = item.getParent();
                            while (p != null) {
                                p.setExpanded(true);
                                p = p.getParent();
                            }
                            // update selected treeitem based on the picked mesh 
                            hierarachyTreeTable.getSelectionModel().selectedItemProperty().removeListener(treeListener);
                            hierarachyTreeTable.getSelectionModel().select(item);
                            hierarachyTreeTable.scrollTo(hierarachyTreeTable.getSelectionModel().getSelectedIndex());
                            hierarachyTreeTable.getSelectionModel().selectedItemProperty().addListener(treeListener);
                            e.consume();
                        });
                        t.getChildren().add(item);
                    });
            });
            
            Group group = new Group();
            group.setId(name);
            treeIndex = new TreeItem<>(group);
            treeIndex.getChildren().addAll(treeTitle);
        }
        
        private String getFirstName(String id){
            return id.split("\\s")[0];
        }
        public TreeItem<Node> getTreeIndex(){ return treeIndex; }
    }
    
    public TreeTableView<Node> getHierarchyTable() { return hierarachyTreeTable; }
    
}
