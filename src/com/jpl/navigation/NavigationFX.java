package com.jpl.navigation;

import com.javafx.experiments.jfx3dviewer.SubSceneResizer;
import com.jpl.navigation.model.Library;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author Jose Pereda - @JPeredaDnr
 * Created on September - 2014
 */
public class NavigationFX extends Application {
    
    private Library library;
    
    @Override
    public void start(Stage stage) {
        
        library=new Library();

        Accordion control = new Accordion();
        control.setOpacity(0.5);
        
        TitledPane controlSettings=new TitledPane("Settings",library.getSettingsTable());
        controlSettings.setOpacity(0.8);
        controlSettings.setExpanded(false);
        control.getPanes().add(controlSettings);

        TitledPane controlMesh=new TitledPane("Mesh Hierarchy",library.getHierarchyTable());
        controlMesh.setOpacity(0.8);
        controlMesh.setExpanded(false);
        controlMesh.expandedProperty().addListener((ov,b,b1)->{
            if(!b1){
                library.clearSelection();
            }
        });
        control.getPanes().add(controlMesh);
        
        control.setOnMouseEntered(e->control.setOpacity(1));
        control.setOnMouseExited(e->control.setOpacity(0.5));
        
        SubSceneResizer stack=new SubSceneResizer(library.getSubScene(),control);
        
        final Scene scene = new Scene(stack, 1024, 800, true);
        
        scene.setFill(Color.ALICEBLUE);
        stage.setTitle("Menéndez Pelayo Library  - JavaFX3D");
        scene.setOnKeyPressed(ev->{
            library.getSubScene().fireEvent(ev);
            ev.consume();
        });
        
        scene.cursorProperty().bind(library.getCursor());
        stage.setScene(scene);
        stage.show();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
